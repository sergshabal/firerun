package firelib.spydia

import java.time.ZonedDateTime

import firelib.common.ModelInitResult
import firelib.common.misc.RollingQuantile
import firelib.common.model.BasketModel
import firelib.common.timeseries.OhlcSeries

/**
 * sample pair trade strategy, it shorts Spy and longs Dia when diverge for last 48 hours
 * more than usual (.9 quantile for last 100 measurements)
 */
class SpyDiaPair extends BasketModel{

    /**
     * we trade just at specified hour
     * this parameter is optimized
     */
    var tradingHour = 0

    /**
     * utility class to calculate rolling quantile over last history
     */
    val highQuantile = new RollingQuantile(quantile = 0.9,length = 100)

    /**
     * capital that is involved in trading
     */
    var money = 5000;


    override protected def applyProperties(mprops: Map[String, String]): ModelInitResult = {
        Min60.enableOhlcAndListen(onHour)
        closePositionAfter(1.day,0,Min60)
        closePositionAfter(1.day,1,Min60)
        tradingHour=mprops("trading.hour").toInt
        ModelInitResult.Success
    }

    def onHour(tss : Seq[OhlcSeries]) : Unit = {

        val spy = tss(0)
        val dia = tss(1)
        /**
         * convert to ny timezone as we are trading just at specified hour
         */
        val localTime: ZonedDateTime = currentTime.toNyTime

        if(!spy(0).interpolated  && localTime.getHour == tradingHour){
            /**
             * calculating metric that shows how spy and dia diverge for last 48 hours
             */
            val metric = (spy(0).C/dia(0).C)/(spy(48).C/dia(48).C)

            /**
             * if metric exceeds high quantile enter into position
             */
            if(metric > highQuantile.value){
                orderManagers(0).managePosTo(-money/spy(0).C.toInt)
                orderManagers(1).managePosTo(money/dia(0).C.toInt)
            }

            if(!metric.isNaN){
                highQuantile.addMetric(metric)
            }
        }
    }
}


