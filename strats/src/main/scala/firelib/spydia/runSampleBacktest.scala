package firelib.spydia

import firelib.common.MarketDataType
import firelib.common.config.{InstrumentConfig, ModelBacktestConfig}
import firelib.common.core.{BacktestMode, backtestStarter}
import firelib.common.opt.OptimizedParameter
import firerun.common.pathHelper

/**
 * backtest runner class
 */

object runSampleBacktest{

    def main(args : Array[String]) ={
        backtestStarter.runBacktest(createSampleBacktestConfig)
    }

    def createSampleBacktestConfig: ModelBacktestConfig = {
        val config: ModelBacktestConfig = new ModelBacktestConfig()
        config.instruments += new InstrumentConfig("SPY", "1MIN/STK/SPY_1.csv", MarketDataType.Ohlc)
        config.instruments += new InstrumentConfig("DIA", "1MIN/STK/DIA_1.csv", MarketDataType.Ohlc)
        config.precacheMarketData = false
        /*
         * optimizing trading.hour parameter
         */
        config.optConfig.params += new OptimizedParameter("trading.hour", 0, 24, 1)
        config.reportTargetPath = pathHelper.reportRoot.resolve("spydia").toAbsolutePath.toString
        config.optConfig.optimizedPeriodDays = 365
        config.modelClassName = classOf[SpyDiaPair].getName
        config.backtestMode = BacktestMode.Optimize
        config.dataServerRoot = pathHelper.dsRoot.toAbsolutePath.toString
        config
    }
}
